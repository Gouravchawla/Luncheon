from django.conf.urls import url
from django.conf import settings
from django.conf.urls.static import static

from rest_framework.urlpatterns import format_suffix_patterns

from . import views


app_name = 'restaurant'
urlpatterns = [
        url(r'^$', views.index, name='index'),
        url(r'^register/$', views.register, name='register'),
        url(r'^login/$', views.user_login, name='login'),
        url(r'^logout/$', views.user_logout, name='logout'),
        url(r'^list/$', views.restaurant_list, name='restaurant_list'),
        url(
            r'^restaurant/(?P<pk>\d+)$',
            views.restaurant_detail,
            name='restaurant_detail'
            ),
        url(
            r'^restaurant/(?P<pk>\d+)/review$',
            views.restaurant_review,
            name='restaurant_review'
            ),
        url(r'^add/$', views.restaurant_add, name='restaurant_add'),
        # API urls
        url(r'^api/v1/city/$', views.CityList.as_view(), name='city_api'),
        ]

urlpatterns = format_suffix_patterns(urlpatterns)

if settings.DEBUG:
    urlpatterns += static(
            settings.MEDIA_URL,
            document_root=settings.MEDIA_ROOT)

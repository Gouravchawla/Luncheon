from django import forms
from django.contrib.auth.models import User

from .models import (
        UserProfile, Restaurant, Review,
        Comment
        )


class UserForm(forms.ModelForm):
    """Form for user registration and User model."""
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class UserProfileForm(forms.ModelForm):
    """Form for user registration and UserProfile model."""
    # city = forms.CharField(widget=forms.Select())

    class Meta:
        model = UserProfile
        fields = ('website', 'picture')


class RestaurantAddForm(forms.ModelForm):
    """Form for adding restaurants in the database."""
    class Meta:
        model = Restaurant
        fields = (
                'name', 'address', 'phone',
                'email', 'website', 'open_time',
                'close_time', 'city',
                )


class ReviewForm(forms.ModelForm):
    """Form for adding review for a restaurant."""
    class Meta:
        model = Review
        fields = (
                'rating',
                'overall_rating',
                'review_text'
                )


class CommentForm(forms.ModelForm):
    """Form for adding review for a restaurant."""
    class Meta:
        model = Comment
        fields = (
                'comment',
                )

from rest_framework import serializers

from .models import City, State, Country


class CitySerializer(serializers.ModelSerializer):

    class Meta:
        model = City
        fields = ('id', 'name')

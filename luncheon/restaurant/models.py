from django.db import models
from django.db.models import Avg
from django.contrib.auth.models import User


class TimeStampedModel(models.Model):
    """An abstract base class model that provides self-updating
    ``created`` and ``modified`` fields.
    """
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Country(models.Model):
    """Stores Countries from around the world."""
    name = models.CharField(
            "Country",
            max_length=50,
            )

    class Meta:
        verbose_name = 'Country'
        verbose_name_plural = 'Countries'

    def __str__(self):
        return self.name


class State(models.Model):
    """Stores State names from around the world."""
    name = models.CharField(
            "State",
            max_length=50,
            )
    country = models.ForeignKey(
            'Country',
            on_delete=models.CASCADE,
            )

    def __str__(self):
        return self.name


class City(models.Model):
    """Stores City names from around the world."""
    name = models.CharField(
            "City",
            max_length=50,
            )
    state = models.ForeignKey(
            'State',
            on_delete=models.CASCADE,
            )

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'

    def __str__(self):
        return self.name


class Restaurant(TimeStampedModel):
    """Stores restaurant information."""
    name = models.CharField(max_length=100,)
    image = models.ImageField(upload_to='restaurant_image')
    phone = models.CharField(max_length=20,)
    email = models.EmailField(blank=True)
    website = models.URLField(blank=True)
    open_time = models.TimeField(blank=True, null=True)
    close_time = models.TimeField(blank=True, null=True)
    address = models.CharField(max_length=255,)
    city = models.ForeignKey(
            'City',
            on_delete=models.CASCADE,
            )

    def average_rating(self):
        """Return the average rating for a restaurant."""
        restaurant_reviews = self.review_set.all()
        restaurant_average_rating = restaurant_reviews.aggregate(
                Avg('rating'))['rating__avg']
        return restaurant_average_rating

    def available_food_categories(self):
        """Returns all the food categories for a restaurant."""
        restaurant_category = self.menuitem_set.all()
        categories = ', '.join(
                set([c.category.name for c in restaurant_category])
                )
        return categories

    def __str__(self):
        return self.name


class FoodCategory(TimeStampedModel):
    """Stores categories of food."""
    name = models.CharField(
            "Category",
            max_length=255,
            )

    class Meta:
        verbose_name = 'Food Category'
        verbose_name_plural = 'Food Categories'

    def __str__(self):
        return self.name


class MenuItem(TimeStampedModel):
    """Stores food items served by restaurants."""
    name = models.CharField(max_length=150,)
    price = models.DecimalField(
            max_digits=10,
            decimal_places=2,
            )
    category = models.ForeignKey(
            'FoodCategory',
            on_delete=models.CASCADE
            )
    restaurant = models.ForeignKey(
            'Restaurant',
            on_delete=models.CASCADE
            )

    def __str__(self):
        return self.name


class UserProfile(TimeStampedModel):
    """Stores User information."""
    user = models.OneToOneField(User, related_name='profile')
    address = models.CharField(
            blank=True,
            help_text="It would help us give better restaurant\
            recommendations.",
            max_length=255,
            )
    city = models.ForeignKey(
            'City',
            on_delete=models.CASCADE,
            blank=True,
            null=True
            )
    website = models.URLField(blank=True)
    picture = models.ImageField(
            upload_to='profile_images',
            blank=True,
            )
    phone = models.CharField(
            max_length=15,
            blank=True)
    bio = models.TextField(
            max_length=170,
            blank=True
            )

    def __str__(self):
        return self.user.username


class Review(TimeStampedModel):
    """Stores restaurant review."""
    RATING_CHOICES = (
            (1, '1'),
            (2, '2'),
            (3, '3'),
            (4, '4'),
            (5, '5'),
            )
    rating = models.IntegerField(
            choices=RATING_CHOICES,
            )
    overall_rating = models.BooleanField(
            verbose_name='Like',
            )
    review_text = models.TextField(
            verbose_name='Review',
            )
    restaurant = models.ForeignKey(
            'Restaurant',
            on_delete=models.CASCADE,
            )
    user = models.ForeignKey(
            User,
            on_delete=models.CASCADE
            )

    class Meta:
        unique_together = ('restaurant', 'user')

    def __str__(self):
        return self.review_text


class Comment(TimeStampedModel):
    """Stores comments on reviews by user."""
    comment = models.TextField()
    review = models.ForeignKey(
            'Review',
            on_delete=models.CASCADE
            )
    user = models.ForeignKey(
            User,
            on_delete=models.CASCADE
            )

    def __str__(self):
        return self.comment


class Visit(models.Model):
    """Stores if user has visited a restaurant or not.
    If restaurant_id and user_id are present then
    the user has visited otherwise not.
    """
    restaurant = models.ForeignKey(
            'Restaurant',
            on_delete=models.CASCADE
            )
    user = models.ForeignKey(
            User,
            on_delete=models.CASCADE
            )

from django.contrib import admin

from .models import (
        Country, State, City,
        Restaurant,
        FoodCategory,
        MenuItem,
        Review,
        Comment,
        UserProfile
        )


class UserAdmin(admin.ModelAdmin):
    list_display = ('user_email', 'city')

    def user_email(self, instance):
        return instance.user.email


admin.site.register(UserProfile, UserAdmin)

admin.register(
        Country, State, City,
        Restaurant,
        FoodCategory,
        MenuItem,
        Review,
        Comment,
        )(admin.ModelAdmin)

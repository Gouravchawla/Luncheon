from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.db import IntegrityError
from django.db.models import Q, Avg

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .serializers import CitySerializer
from .models import Restaurant, City
from .forms import (
        UserForm, UserProfileForm,
        RestaurantAddForm, ReviewForm,
        CommentForm
        )


def register(request):
    """Handles user registration."""

    if request.method == 'POST':
        # Grab information from the raw form - both UserForm and
        # and UserProfileForm
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)

        # If the two forms are valid, process further
        if user_form.is_valid() and profile_form.is_valid():
            # Save the user form data to the database.
            user = user_form.save()

            # Now we hash the password with the set_password method.
            # Once hashed, we can update the user object.
            user.set_password(user.password)
            user.save()

            # Now sort out the UserProfile instance.
            # Since we need to set the user attribute ourselves, we set
            # commit=False.
            #city = get_object_or_404(
            #        City,
            #        pk=profile_form.cleaned_data.get('city')
            #        )
            profile = profile_form.save(commit=False)
            profile.user = user
            #profile.city = city

            # Process the profile picture, if present
            if 'picture' in request.FILES:
                profile.picture = request.FILES['picture']

            # Save the UserProfile model instance.
            profile.save()

            # Set registration to True to tell the template registration was
            # successful.
            messages.success(
                    request,
                    'Your account has been successfully created.'
                    )

        else:
            print(user_form.errors, profile_form.errors)
            messages.warning(
                    request,
                    'Please correct the error(s) and submit again.'
                    )
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()

    return render(
            request,
            'restaurant/register.html',
            {
                'user_form': user_form,
                'profile_form': profile_form,
                }
            )


def user_login(request):
    """Handles login."""
    print(request.POST)
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        # Using Django's method to authenticate user.
        # If username/password combination is valid - a User object
        # is retured.
        user = authenticate(username=username, password=password)

        # If we have a user object then the credentials were correct
        # else None is returned
        if user:
            # Checking if account is active
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/')
            else:
                messages.warning(
                        request,
                        'Your account has been disabled'
                        )
                return render(request, 'restaurant/login.html', {})
        else:
            messages.warning(
                    request,
                    'Invalid credentials. Try again.'
                    )
            return render(request, 'restaurant/login.html', {})
    else:
        return render(request, 'restaurant/login.html', {})


@login_required
def user_logout(request):
    """Handles logout."""
    logout(request)

    return HttpResponseRedirect('/')


def index(request):
    """Lists restaurants based on user's current location."""
    # Get the latest 3 restaurant
    restaurant_list = Restaurant.objects.all().order_by('-created')[:3]
    cities = City.objects.select_related().filter(
            state__country__name='India'
            )
    return render(
            request, 'restaurant/index.html',
            {
                'restaurants': restaurant_list,
                'cities': cities,
                }
            )


def restaurant_list(request):
    """Lists all restaurants or based on user search criteria."""
    restaurant_list = Restaurant.objects.all()
    context = {}
    query = request.GET.get('query')
    city = request.GET.get('city', None)
    cities = City.objects.select_related().all()
    # If we get a blank query, we show all restaurants in a city
    if city and not query:
        city_queryset = cities.get(id=city)
        context.update({'city': city_queryset.name})
        restaurant_list = restaurant_list.filter(
                Q(city=city) |
                Q(address__icontains=city_queryset.name)
                )
    # If we get a query then we show restaurants which contains the
    # keyword and are in the searched city.
    if query:
        restaurant_list = restaurant_list.filter(
                Q(city=city) &
                Q(name__icontains=query) |
                Q(menuitem__name__icontains=query) |
                Q(menuitem__category__name__icontains=query)
                ).distinct()
    context.update(
            {
                'restaurant_list': restaurant_list,
                'cities': cities,
                'query': query
                }
            )
    return render(
            request,
            'restaurant/restaurant_list.html',
            context
            )


@login_required
def restaurant_add(request):
    """Handles the restaurant add feature."""
    if request.method == 'POST':
        restaurant_add_form = RestaurantAddForm(data=request.POST)

        # Check if the form is valid
        if restaurant_add_form.is_valid():
            restaurant_add_form.save()
            return HttpResponseRedirect('/')
        else:
            print(restaurant_add_form.errors)
    else:
        restaurant_add_form = RestaurantAddForm()

    return render(
            request,
            'restaurant/restaurant_add.html',
            {'restaurant_add_form': restaurant_add_form}
            )


def restaurant_detail(request, pk):
    """Handles details page of restaurant."""
    restaurant = get_object_or_404(Restaurant.objects.select_related(), pk=pk)
    restaurant_menu = restaurant.menuitem_set.all()
    restaurant_reviews = restaurant.review_set.all()
    restaurant_average_rating = restaurant_reviews.aggregate(
            Avg('rating'))['rating__avg']
    print(restaurant, restaurant_menu, restaurant_reviews)
    context = {
            'restaurant': restaurant,
            'restaurant_menu': restaurant_menu,
            'restaurant_reviews': restaurant_reviews,
            'restaurant_average_rating': restaurant_average_rating
            }
    return render(
            request,
            'restaurant/restaurant_detail.html',
            context
            )


def restaurant_review(request, pk):
    """Handles reviews page of restaurant."""
    # Set save to True if registratio is succesful
    review_save = False
    restaurant = get_object_or_404(Restaurant.objects.select_related(), pk=pk)
    restaurant_reviews = restaurant.review_set.all()
    restaurant_average_rating = restaurant_reviews.aggregate(
            Avg('rating'))['rating__avg']
    if request.method == 'POST':
        review_form = ReviewForm(data=request.POST)
        if review_form.is_valid():
            try:
                form = review_form.save(commit=False)
                form.user = request.user
                form.restaurant = restaurant
                form.save()
                messages.success(
                        request,
                        'You are review has been adedd succesful'
                        )
            except IntegrityError:
                messages.warning(
                        request,
                        'You can only add one review per restaurant.'
                        )
                pass
        else:
            print(review_form.errors)
    else:
        review_form = ReviewForm()
    context = {
            'restaurant': restaurant,
            'restaurant_reviews': restaurant_reviews,
            'restaurant_average_rating': restaurant_average_rating,
            'review_form': review_form,
            'review_save': review_save,
            }
    return render(
            request,
            'restaurant/restaurant_review.html',
            context
            )


# API views
class CityList(APIView):
    """Returns city based on query string."""

    def get(self, request):
        # Look for the query string term
        term = request.GET.get('term')
        if term:
            cities = City.objects.filter(name__icontains=term)
            serializer = CitySerializer(cities, many=True)
            return Response(serializer.data)
        else:
            return Response([])

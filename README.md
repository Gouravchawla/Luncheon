# Luncheon
Luncheon is a restaurant discovery app.

## Setup instructions:

- Create a Python3 virtual enviornment
- Clone the repo
- Install requirements: `pip install -r requirements.txt`
- Create a MySQL database named `luncheon`
- Enter `username` & `password` of your MySQL database in `settings.py`
- Run migrations: `python manage.py makemigrations restaurant` & `python manage.py migrate`
- Create two users/superusers (required for demo data)
- Run fixture: `python manage.py loaddata demo.json`

## Features:

- Add restaurant by searching (Uses Google places API and Maps Geocoding API for geocoding and reverse geocoding along with autocomplete)
- Restaurant Search (by city, cuisine, keyword)
- Restaurant reviews
- Restaurant Menu
- User Registration, Login, Logout
- [WIP]Comment on Reviews
- [WIP]User restaurant visit history
- [WIP]Restaurant removal from search result if user didn't like the restaurant before
- [WIP] User profile page
- API using Django Rest Framework (Available endpoint: /api/v1/city)

## ER Diagram
![ER Diagram](ER_diagram.png)
